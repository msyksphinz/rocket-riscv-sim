#!/bin/bash

BASE_DIR=`pwd`
VENDOR_DIR=vendor/freechips_boomv2
V_DIR=${VENDOR_DIR}/emulator/generated-src
TARGET_DIR=rtl/freechips.boom2
DESIGN_NAME=rocketchip.BOOMConfig

BOOMV2_SUBMODULES=(chisel3 firrtl hardfloat torture boom)

pushd ${VENDOR_DIR}
git checkout -- .
git checkout boom
git submodule update --init --recursive ${BOOMV2_SUBMODULES}

patch -p1 < ${BASE_DIR}/patch/freechips_boomv2.patch

cd boom
git checkout -- .
git submodule update --init --recursive
git checkout v2.0.1

patch -p1 < ${BASE_DIR}/patch/freechips_boomv2_core.patch

cd ../emulator
make -j4 CONFIG=BOOMConfig
popd

mkdir -p ${TARGET_DIR}
gzip -f ${V_DIR}/${DESIGN_NAME}.v
cp ${V_DIR}/${DESIGN_NAME}.v.gz ${TARGET_DIR}
# cp ${V_DIR}/${DESIGN_NAME}.rom.v ${TARGET_DIR}
