#!/bin/bash

V_DIR=freedom/builds/e300artydevkit
TARGET_DIR=rtl/sifive_freedom_everywhere_e300artydevkit/

pushd freedom
make -f Makefile.e300artydevkit clean
make -f Makefile.e300artydevkit verilog
make -f Makefile.e300artydevkit romgen
popd

gzip ${V_DIR}/sifive.freedom.everywhere.e300artydevkit.E300ArtyDevKitConfig.v
cp ${V_DIR}/sifive.freedom.everywhere.e300artydevkit.E300ArtyDevKitConfig.v.gz ${TARGET_DIR}
cp ${V_DIR}/sifive.freedom.everywhere.e300artydevkit.E300ArtyDevKitConfig.rom.v ${TARGET_DIR}
