#!/bin/sh

V_DIR=freedom/builds/u500vc707devkit
TARGET_DIR=rtl/sifive_freedom_everywhere_u500vc707devkit
DESIGN_NAME=sifive.freedom.unleashed.u500vc707devkit.U500VC707DevKitConfig

pushd freedom
make -f Makefile.u500vc707devkit clean
make -f Makefile.u500vc707devkit verilog
make -f Makefile.u500vc707devkit romgen
popd

gzip ${V_DIR}/${DESIGN_NAME}.v
cp ${V_DIR}/${DESIGN_NAME}.v.gz ${TARGET_DIR}
cp ${V_DIR}/${DESIGN_NAME}.rom.v ${TARGET_DIR}
