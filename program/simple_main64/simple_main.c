int simple_main ()
{

  (*(volatile unsigned int *)0x80000400) = 0xdeadbeef;

  int total = 0;
  for (int i = 0; i < 100; i++) {
	if ((i % 3) == 0) total += 3;
	if ((i % 5) == 0) total += 5;
	total += i;
  }

  (*(volatile int *)0x80000404) = total;

  return 0;
}

	
