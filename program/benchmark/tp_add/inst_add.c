#include <stdint.h>
int main () {
  int64_t  out0;
  int64_t  out1;
  int64_t  out2;
  int64_t  out3;
  int64_t  out4;
  int64_t  out5;
  int64_t  out6;
  int64_t  out7;
  int64_t  out8;
  int64_t  out9;
  int64_t  out10;
  int64_t  out11;
  int64_t  out12;
  int64_t  out13;
  int64_t  out14;
  int64_t  out15;
  int64_t  in0_0 = 1333421358;
  int64_t  in1_0 = 1587194397;
  int64_t  in2_0 = 1293899489;
  int64_t  in3_0 = 2220669387;
  int64_t  in4_0 = 2214091311;
  int64_t  in5_0 = 1174936842;
  int64_t  in6_0 = 4251482359;
  int64_t  in7_0 = 2958687221;
  int64_t  in8_0 = 3291417395;
  int64_t  in9_0 = 1224296866;
  int64_t  in10_0 = 31293180;
  int64_t  in11_0 = 3171776044;
  int64_t  in12_0 = 2899225709;
  int64_t  in13_0 = 1270991819;
  int64_t  in14_0 = 3891840867;
  int64_t  in15_0 = 2628134458;
  int64_t  in0_1 = 2078584430;
  int64_t  in1_1 = 86010001;
  int64_t  in2_1 = 4256832673;
  int64_t  in3_1 = 670655472;
  int64_t  in4_1 = 3262129529;
  int64_t  in5_1 = 894570932;
  int64_t  in6_1 = 1483566619;
  int64_t  in7_1 = 2630591905;
  int64_t  in8_1 = 2879158385;
  int64_t  in9_1 = 2556638655;
  int64_t  in10_1 = 67180364;
  int64_t  in11_1 = 133069935;
  int64_t  in12_1 = 1869346655;
  int64_t  in13_1 = 2400131768;
  int64_t  in14_1 = 4059644932;
  int64_t  in15_1 = 2341707550;
  int i;
  for (i = 0; i < 32; i++) {
    asm volatile ("add        %0,%1,%2"
				  :"=r"(out0)
				  :"r"(in0_0),"r"(in0_1)
				  :
				  );
    asm volatile ("add        %0,%1,%2"
				  :"=r"(out1)
				  :"r"(in1_0),"r"(in1_1)
				  :
				  );
    asm volatile ("add        %0,%1,%2"
				  :"=r"(out2)
				  :"r"(in2_0),"r"(in2_1)
				  :
				  );
    asm volatile ("add        %0,%1,%2"
				  :"=r"(out3)
				  :"r"(in3_0),"r"(in3_1)
				  :
				  );
    asm volatile ("add        %0,%1,%2"
				  :"=r"(out4)
				  :"r"(in4_0),"r"(in4_1)
				  :
				  );
    asm volatile ("add        %0,%1,%2"
				  :"=r"(out5)
				  :"r"(in5_0),"r"(in5_1)
				  :
				  );
    asm volatile ("add        %0,%1,%2"
				  :"=r"(out6)
				  :"r"(in6_0),"r"(in6_1)
				  :
				  );
    asm volatile ("add        %0,%1,%2"
				  :"=r"(out7)
				  :"r"(in7_0),"r"(in7_1)
				  :
				  );
  }
  (*(volatile int64_t  *)(0x80001000 + 	0)) = out0;
  (*(volatile int64_t  *)(0x80001000 + 	8)) = out1;
  (*(volatile int64_t  *)(0x80001000 + 16)) = out2;
  (*(volatile int64_t  *)(0x80001000 + 32)) = out3;
  (*(volatile int64_t  *)(0x80001000 + 40)) = out4;
  (*(volatile int64_t  *)(0x80001000 + 48)) = out5;
  (*(volatile int64_t  *)(0x80001000 + 56)) = out6;
  (*(volatile int64_t  *)(0x80001000 + 64)) = out7;
}
