#include <stdint.h>

int benchmark ();

__attribute__((section(".text.startup")))

#define MSTATUS_FS          0x00006000

void start ()
{
  asm volatile ("csrs mstatus, %0":
				:"r"(MSTATUS_FS & (MSTATUS_FS >> 1)));
  asm volatile ("csrwi fcsr, 0");
  
  asm volatile ("csrw mepc, %0"
				: :"r"(benchmark));
  asm volatile ("mret");
}

__attribute__((section(".text")))

int benchmark () {
  float register out0;
  float register out1;
  float register out2;
  float register out3;
  float register out4;
  float register out5;
  float register out6;
  float register out7;
  float register out8;
  float register out9;
  float register out10;
  float register out11;
  float register out12;
  float register out13;
  float register out14;
  float register out15;
  float register in0_0 = 13334.21358;
  float register in1_0 = 15871.94397;
  float register in2_0 = 12938.99489;
  float register in3_0 = 22206.69387;
  float register in0_1 = 2078.584430;
  float register in1_1 = 8601.0001;
  float register in2_1 = 4256.832673;
  float register in3_1 = 6706.55472;
  int i;
  for (i = 0; i < 32; i++) {
    asm volatile (
				  "fadd.s %[w00],%[a00],%[b00]; \n"
				  "fadd.s %[w01],%[a01],%[w00]; \n"
				  "fadd.s %[w02],%[a02],%[w01]; \n"
				  "fadd.s %[w03],%[a03],%[w02]; \n"
				  "fadd.s %[w04],%[a00],%[w03]; \n"
				  "fadd.s %[w05],%[a01],%[w04]; \n"
				  "fadd.s %[w06],%[a02],%[w05]; \n"
				  "fadd.s %[w07],%[a03],%[w06]; \n"
				  "fadd.s %[w08],%[a00],%[w07]; \n"
				  "fadd.s %[w09],%[a01],%[w08]; \n"
				  "fadd.s %[w10],%[a02],%[w09]; \n"
				  "fadd.s %[w11],%[a03],%[w10]; \n"
				  "fadd.s %[w12],%[a00],%[w11]; \n"
				  "fadd.s %[w13],%[a01],%[w12]; \n"
				  "fadd.s %[w14],%[a02],%[w13]; \n"
				  "fadd.s %[w15],%[a03],%[w14]; \n"
				  : [w00] "=f"(out0), [w01] "=f"(out1), [w02] "=f"(out2), [w03] "=f"(out3),
					[w04] "=f"(out4), [w05] "=f"(out5), [w06] "=f"(out6), [w07] "=f"(out7),
					[w08] "=f"(out8), [w09] "=f"(out9), [w10] "=f"(out10), [w11] "=f"(out11),
					[w12] "=f"(out12), [w13] "=f"(out13), [w14] "=f"(out14), [w15] "=f"(out15)
				  : [a00] "f" (in0_0 ), [b00] "f" (in0_1 ),
					[a01] "f" (in1_0 ),
					[a02] "f" (in2_0 ),
					[a03] "f" (in3_0 )
				  );

	
  }
  (*(volatile float  *)(0x80001000 + 	0)) = out0;
  (*(volatile float  *)(0x80001000 + 	8)) = out1;
  (*(volatile float  *)(0x80001000 + 16)) = out2;
  (*(volatile float  *)(0x80001000 + 32)) = out3;
  (*(volatile float  *)(0x80001000 + 40)) = out4;
  (*(volatile float  *)(0x80001000 + 48)) = out5;
  (*(volatile float  *)(0x80001000 + 56)) = out6;
  (*(volatile float  *)(0x80001000 + 64)) = out7;
}

