#include <stdint.h>

void start ();
int benchmark ();

void start ()
{
  asm volatile ("csrw mepc, %0"
				: :"r"(benchmark));
  asm volatile ("mret");
}

 
int benchmark () {
  int64_t  out0;
  int64_t  out1;
  int64_t  out2;
  int64_t  out3;
  int64_t  out4;
  int64_t  out5;
  int64_t  out6;
  int64_t  out7;
  int64_t  out8;
  int64_t  out9;
  int64_t  out10;
  int64_t  out11;
  int64_t  out12;
  int64_t  out13;
  int64_t  out14;
  int64_t  out15;
  int64_t  in0_0 = 1333421358;
  int64_t  in1_0 = 1587194397;
  int64_t  in2_0 = 1293899489;
  int64_t  in3_0 = 2220669387;
  int64_t  in0_1 = 2078584430;
  int64_t  in1_1 = 86010001;
  int64_t  in2_1 = 4256832673;
  int64_t  in3_1 = 670655472;

  int i;
  for (i = 0; i < 32; i++) {
    asm volatile (
				  "mul %[w00],%[a00],%[b00]; \n"
				  "mul %[w01],%[a01],%[b01]; \n"
				  "mul %[w02],%[a02],%[b02]; \n"
				  "mul %[w03],%[a03],%[b03]; \n"
				  "mul %[w04],%[a00],%[b00]; \n"
				  "mul %[w05],%[a01],%[b01]; \n"
				  "mul %[w06],%[a02],%[b02]; \n"
				  "mul %[w07],%[a03],%[b03]; \n"
				  "mul %[w08],%[a00],%[b00]; \n"
				  "mul %[w09],%[a01],%[b01]; \n"
				  "mul %[w10],%[a02],%[b02]; \n"
				  "mul %[w11],%[a03],%[b03]; \n"
				  "mul %[w12],%[a00],%[b00]; \n"
				  "mul %[w13],%[a01],%[b01]; \n"
				  "mul %[w14],%[a02],%[b02]; \n"
				  "mul %[w15],%[a03],%[b03]; \n"
				  : [w00] "=r"(out0), [w01] "=r"(out1), [w02] "=r"(out2), [w03] "=r"(out3),
					[w04] "=r"(out4), [w05] "=r"(out5), [w06] "=r"(out6), [w07] "=r"(out7),
					[w08] "=r"(out8), [w09] "=r"(out9), [w10] "=r"(out10), [w11] "=r"(out11),
					[w12] "=r"(out12), [w13] "=r"(out13), [w14] "=r"(out14), [w15] "=r"(out15)
				  : [a00] "r" (in0_0 ), [b00] "r" (in0_1 ),
					[a01] "r" (in1_0 ), [b01] "r" (in1_1 ),
					[a02] "r" (in2_0 ), [b02] "r" (in2_1 ),
					[a03] "r" (in3_0 ), [b03] "r" (in3_1 )
				  );

	
  }
  (*(volatile int64_t  *)(0x80001000 + 	0)) = out0;
  (*(volatile int64_t  *)(0x80001000 + 	8)) = out1;
  (*(volatile int64_t  *)(0x80001000 + 16)) = out2;
  (*(volatile int64_t  *)(0x80001000 + 32)) = out3;
  (*(volatile int64_t  *)(0x80001000 + 40)) = out4;
  (*(volatile int64_t  *)(0x80001000 + 48)) = out5;
  (*(volatile int64_t  *)(0x80001000 + 56)) = out6;
  (*(volatile int64_t  *)(0x80001000 + 64)) = out7;
}
