#!/usr/bin/ruby
#
# Copyright (c) 2015, Masayuki Kimura
# All rights reserved.
#
#     Redistribution and use in source and binary forms, with or without
#     modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the Masayuki Kimura nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL MASAYUKI KIMURA BE LIABLE FOR ANY
#     DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#      LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#     ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

require './riscv_arch_table.rb'
require 'fileutils'

##=== displaying headers ===

class File
  def gen_header()
    self.puts("/*")
    self.puts(" * Copyright (c) 2015, Masayuki Kimura")
    self.puts(" * All rights reserved.")
    self.puts(" *")
    self.puts(" *     Redistribution and use in source and binary forms, with or without")
    self.puts(" *     modification, are permitted provided that the following conditions are met:")
    self.puts(" *     * Redistributions of source code must retain the above copyright")
    self.puts(" *     notice, this list of conditions and the following disclaimer.")
    self.puts(" *     * Redistributions in binary form must reproduce the above copyright")
    self.puts(" *     notice, this list of conditions and the following disclaimer in the")
    self.puts(" *       documentation and/or other materials provided with the distribution.")
    self.puts(" *     * Neither the name of the Masayuki Kimura nor the")
    self.puts(" *       names of its contributors may be used to endorse or promote products")
    self.puts(" *       derived from this software without specific prior written permission.")
    self.puts(" *")
    self.puts(" * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\" AND")
    self.puts(" *     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED")
    self.puts(" * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE")
    self.puts(" * DISCLAIMED. IN NO EVENT SHALL MASAYUKI KIMURA BE LIABLE FOR ANY")
    self.puts(" *     DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES")
    self.puts(" *     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;")
    self.puts(" *      LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND")
    self.puts(" *     ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT")
    self.puts(" *     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS")
    self.puts(" *     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.")
    self.puts(" */")
    self.puts("")
    self.puts("/* CAUTION! THIS SOURCE CODE IS GENERATED AUTOMATICALLY. DON'T MODIFY BY HAND. */")
    self.puts("")
    self.puts("")
  end
end

module DEC
  FUNC_STR = $arch_list_def["TAIL"]
  CURR_DEC = $arch_list_def["TAIL"] + 1
  INST_NAME = $arch_list_def["TAIL"] + 2
end

LOOP_NUM = 256
GEN_INST_NUM = 6

def add_fp_enable(fp)
  fp.puts ("void benchmark () __attribute__((section(\".text\")));\n\n")
  fp.puts ("void run_bench () __attribute__((section(\".text\")));\n\n")

  fp.puts ("void start () __attribute__((section(\".text.startup\")));\n")
  fp.puts ("\#define MSTATUS_FS          0x00006000\n")
  fp.puts ("void start ()\n")
  fp.puts ("{\n")
  fp.puts ("  asm volatile (\"csrs mstatus, \%0\":\n")
  fp.puts ("                :\"r\"(MSTATUS_FS & (MSTATUS_FS >> 1)));\n")
  fp.puts ("  asm volatile (\"csrwi fcsr, 0\");\n")
  fp.puts ("run_bench();\n")
  # fp.puts ("  asm volatile (\"csrw mepc, \%0\"\n")
  # fp.puts ("                : :\"r\"(run_bench));\n")
  # fp.puts ("  asm volatile (\"mret\");\n")
  fp.puts ( "}\n\n")
end


def add_finish(fp)
  fp.puts ("void finish () __attribute__((section(\".text\")));\n")
  fp.puts ("void finish ()\n")
  fp.puts ("{\n")
  fp.puts ("  asm volatile (\"csrr    s0, mhartid\");")
  fp.puts ("  asm volatile (\"sw      s0, 264(zero)\");")
  fp.puts ("  asm volatile (\"csrr    s0, dscratch\");")
  fp.puts ("  asm volatile (\"dret\");")
  fp.puts ("}\n\n")
end


def add_run_bench(fp)
  fp.puts("void run_bench ()\n")
  fp.puts("{benchmark (); benchmark(); finish(); }\n\n")
end


def gen_linker(fp)
  fp.puts ("SECTIONS\n")
  fp.puts ("{\n")
  fp.puts ("    ROM_BASE = 0x80000000; /* ... but actually position independent */\n")
  fp.puts ("\n")
  fp.puts ("    . = ROM_BASE;\n")
  fp.puts ("    .text : {\n")
  fp.puts ("        *(.text.startup)\n")
  fp.puts ("        *(.text)\n")
  fp.puts ("    }\n")
  fp.puts ("    .rodata : { *(.rodata) }\n")
  fp.puts ("}\n")
end


def gen_variable_type(reg_type)
  if reg_type == 'x' then
    out_reg_type = "int "
  elsif reg_type == 'f' then
    out_reg_type = "float"
  elsif reg_type == 'd' then
    out_reg_type = "double"
  else
    printf("ERROR: unknown register type %s.\n\n", reg_type)
  end
  return out_reg_type
end # gen_variable_type

def gen_gas_reg_type(reg_type)
  if reg_type == 'x' then
    out_reg_type = "r"
  elsif reg_type == 'f' then
    out_reg_type = "f"
  elsif reg_type == 'd' then
    out_reg_type = "f"
  else
    printf("ERROR: unknown register type %s.\n\n", reg_type)
  end
  return out_reg_type
end # gen_gas_reg_type


def gen_random_num_str (reg_type)
  if reg_type == 'x' then
    # out_num = rand(1024).to_s
    out_num = "1";
  elsif reg_type == 'f' then
    # out_num = (rand / rand).to_s
    out_num = "1.0"
  elsif reg_type == 'd' then
    # out_num = (rand / rand).to_s
    out_num = "1.0"
  else
    printf("ERROR: unknown register type %s.\n\n", reg_type)
  end
  return out_num
end

def gen_hex (filename)
  system_str = ""
  system_str += sprintf("riscv64-unknown-elf-gcc -O3 -fpic -fno-common -funroll-loops -nostartfiles %s -Tisa_rocket/linker.ld -o %s.elf\n",
                        filename, filename.gsub(".c", ""))
  system_str += sprintf("riscv64-unknown-elf-objdump -D %s.elf > %s.dmp\n",
                        filename.gsub(".c", ""), filename.gsub(".c", ""))
  system_str += sprintf("riscv64-unknown-elf-objcopy -O binary %s.elf %s.bin\n",
                        filename.gsub(".c", ""), filename.gsub(".c", ""))
  system_str += sprintf("od -t x8 -An -w8 -v %s.bin > %s.hex\n",
                        filename.gsub(".c", ""), filename.gsub(".c", ""))
  system(system_str)
end

FileUtils.rm_rf("isa_rocket")
Dir.mkdir("isa_rocket", 0755)

linker_fp = File.open("isa_rocket/linker.ld", "w")
gen_linker(linker_fp)
linker_fp.close


def gen_pattern(pat_type, filename, inst_info)
  isa_fp = File.open(filename, 'w')

  isa_fp.gen_header() # making header
  isa_fp.puts("#include <stdint.h>\n")
  add_fp_enable(isa_fp)
  add_finish(isa_fp)
  add_run_bench(isa_fp)
  isa_fp.puts("void benchmark () {\n")

  regtype_list = inst_info[$arch_list_def["NAME"]].scan(/\w\[\d+:\d+\]/).map{|reg_desc|
    reg_desc.gsub(/\[\d+:\d+\]/, "")
  }

  # Generate Destination Variable
  out_reg_type = gen_variable_type(regtype_list[0])
  isa_fp.print(out_reg_type + " register ")
  (GEN_INST_NUM).times{|val_idx|
    isa_fp.printf(" out%02d", val_idx)
    isa_fp.printf(" = %s", gen_random_num_str(regtype_list[0]))
    if val_idx != GEN_INST_NUM-1 then
      isa_fp.print(", ")
    else
      isa_fp.print(";\n")
    end
  }

  # Generate Source Variable
  for num_source in 1..(regtype_list.size-1)
    isa_fp.print("const " + gen_variable_type(regtype_list[num_source]) + " register ")
    for val_idx in 0..GEN_INST_NUM-1
      isa_fp.printf(" in%02d_%02d", num_source, val_idx)
      isa_fp.printf(" = %s", gen_random_num_str(regtype_list[num_source]))
      if val_idx != GEN_INST_NUM-1 then
        isa_fp.print(", ")
      else
        isa_fp.print(";\n")
      end
    end
  end

  isa_fp.puts("int i, start_cycle, stop_cycle;")
  isa_fp.puts("(*(volatile int *)(0x80002000)) = 0x10;\n")
  isa_fp.puts("asm volatile (\"rdcycle %0\": \"=r\"(start_cycle));")
  isa_fp.puts("for (i = 0; i < " + LOOP_NUM.to_s + "; i++) {")

  isa_fp.puts("asm volatile (")
  GEN_INST_NUM.times{|inst_idx|
    reg_idx = 0
    inst_asm = inst_info[$arch_list_def["NAME"]].gsub(/\w\[\d+:\d+\]/) {|word|
      reg_idx += 1
      if reg_idx == 1 then
        "%[out" + sprintf("%02d" ,inst_idx) + "]"
      else
        if pat_type == 'lat' and reg_idx == 2 && inst_idx == 0 then
          "%[out" + sprintf("%02d", GEN_INST_NUM-1) + "]"
        elsif pat_type == 'lat' and reg_idx == 2 && inst_idx != 0 then
          "%[out" + sprintf("%02d", (inst_idx-1)) + "]"
        else
          "%[in" + sprintf("%02d", (reg_idx-1)) + "_" + sprintf("%02d", inst_idx) + "]"
        end
      end
    }
    isa_fp.puts("\"" + inst_asm + "; \\n\"")
  }

  isa_fp.puts(":")

  # Generate Destination Variable
  (GEN_INST_NUM).times{|val_idx|
    isa_fp.printf("[out%02d] \"=&" + gen_gas_reg_type(regtype_list[0]) + "\"(out%02d)", val_idx, val_idx)
    if val_idx != GEN_INST_NUM-1 then
      isa_fp.print(", ")
    end
  }

  isa_fp.puts(":")

  # Generate Source Variable
  for num_source in 1..(regtype_list.size-1)
    for val_idx in 0..GEN_INST_NUM-1
      if pat_type == 'lat' and num_source == 1 and val_idx != 0 then
        isa_fp.printf("[in%02d_%02d] \"" + gen_gas_reg_type(regtype_list[num_source]) + "\"(out%02d)",
                      num_source, val_idx, val_idx-1)
      else
        isa_fp.printf("[in%02d_%02d] \"" + gen_gas_reg_type(regtype_list[num_source]) + "\"(in%02d_%02d)",
                      num_source, val_idx, num_source, val_idx)
      end
      if num_source != regtype_list.size-1 or
          val_idx != GEN_INST_NUM-1 then
        isa_fp.print(", ")
      end
    end
  end

  isa_fp.puts(");\n\n");
  isa_fp.puts("}");

  isa_fp.puts("asm volatile (\"rdcycle %0\": \"=r\"(stop_cycle));")

  GEN_INST_NUM.times{|number_times|
    isa_fp.printf("  (*(volatile %s *)(0x80001000 + %d)) = out%02d;\n", out_reg_type, number_times * 8, number_times)
  }
  isa_fp.puts("  (*(volatile int *)(0x80002000)) = (stop_cycle - start_cycle);\n")

  isa_fp.puts("}\n")
  isa_fp.close()

  gen_hex(filename)

end

$arch_table.each_with_index {|inst_info, index|
  mne = "%s"%([inst_info[$arch_list_def["NAME"]].split(" ")[0].gsub(/\./,'_')])
  filename = 'isa_rocket/tp_' + inst_info[$arch_list_def["TYPE"]] + "_" + mne + '.c'
  gen_pattern('tp', filename, inst_info)

  mne = "%s"%([inst_info[$arch_list_def["NAME"]].split(" ")[0].gsub(/\./,'_')])
  filename = 'isa_rocket/lat_' + inst_info[$arch_list_def["TYPE"]] + "_" + mne + '.c'
  gen_pattern('lat', filename, inst_info)
}
