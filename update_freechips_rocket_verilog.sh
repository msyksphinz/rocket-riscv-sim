#!/bin/bash

V_BASE_DIR=freechips_rocket
V_DIR=${V_BASE_DIR}/emulator/generated-src
TARGET_DIR=rtl/freechips.rocketchip
DESIGN_NAME=freechips.rocketchip.system.DefaultConfig

pushd ${V_BASE_DIR}
git submodule update --init --recursive chisel3 firrtl hardfloat torture
popd

pushd ${V_BASE_DIR}/emulator
make clean && make CONFIG=DefaultConfig 
popd

mkdir -p ${TARGET_DIR}
gzip -f ${V_DIR}/${DESIGN_NAME}.v
cp ${V_DIR}/${DESIGN_NAME}.v.gz ${TARGET_DIR}
# cp ${V_DIR}/${DESIGN_NAME}.behav_srams.v ${TARGET_DIR}
