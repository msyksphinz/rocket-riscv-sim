module tb;
  timeunit 1ns;
  timeprecision 10ps;

  logic clock, reset;
  localparam clk_hperiod = 1ns;

  initial begin
    clock = 1'b1;
    reset = 1'b1;
    #(300);
    reset = 1'b0;
  end

  always #(clk_hperiod) begin
    clock <= !clock;
  end

  TestHarness duv
    (
     .clock (clock),
     .reset (reset),
     .io_success ()
     );

  initial begin
	#(10000000);
    $finish;
  end

  
  always_ff @ (posedge clock) begin
    if (tb.duv.ExampleRocketTop.coreplex.debug.io_in_0_a_valid &&
        tb.duv.ExampleRocketTop.coreplex.debug.io_in_0_a_ready &&
        tb.duv.ExampleRocketTop.coreplex.debug.io_in_0_a_bits_address == 32'h108) begin
      $fwrite (32'h80000002,"ExapmleRocketSystem Debug Asserted. Finished.\n");
      $finish;
    end
  end

  logic [1023:0] tp_hex_file[7:0];
  initial begin
    void'($value$plusargs("tp_hex_0=%s", tp_hex_file[0]));
    void'($value$plusargs("tp_hex_1=%s", tp_hex_file[1]));
    void'($value$plusargs("tp_hex_2=%s", tp_hex_file[2]));
    void'($value$plusargs("tp_hex_3=%s", tp_hex_file[3]));
    void'($value$plusargs("tp_hex_4=%s", tp_hex_file[4]));
    void'($value$plusargs("tp_hex_5=%s", tp_hex_file[5]));
    void'($value$plusargs("tp_hex_6=%s", tp_hex_file[6]));
    void'($value$plusargs("tp_hex_7=%s", tp_hex_file[7]));
    
    $readmemh (tp_hex_file[0], tb.duv.SimAXIMem.AXI4RAM.mem_0);
    $readmemh (tp_hex_file[1], tb.duv.SimAXIMem.AXI4RAM.mem_1);
    $readmemh (tp_hex_file[2], tb.duv.SimAXIMem.AXI4RAM.mem_2);
    $readmemh (tp_hex_file[3], tb.duv.SimAXIMem.AXI4RAM.mem_3);
    $readmemh (tp_hex_file[4], tb.duv.SimAXIMem.AXI4RAM.mem_4);
    $readmemh (tp_hex_file[5], tb.duv.SimAXIMem.AXI4RAM.mem_5);
    $readmemh (tp_hex_file[6], tb.duv.SimAXIMem.AXI4RAM.mem_6);
    $readmemh (tp_hex_file[7], tb.duv.SimAXIMem.AXI4RAM.mem_7);

    $readmemh (tp_hex_file[0], tb.duv.SimAXIMem_1.AXI4RAM.mem_0);
    $readmemh (tp_hex_file[1], tb.duv.SimAXIMem_1.AXI4RAM.mem_1);
    $readmemh (tp_hex_file[2], tb.duv.SimAXIMem_1.AXI4RAM.mem_2);
    $readmemh (tp_hex_file[3], tb.duv.SimAXIMem_1.AXI4RAM.mem_3);
    $readmemh (tp_hex_file[4], tb.duv.SimAXIMem_1.AXI4RAM.mem_4);
    $readmemh (tp_hex_file[5], tb.duv.SimAXIMem_1.AXI4RAM.mem_5);
    $readmemh (tp_hex_file[6], tb.duv.SimAXIMem_1.AXI4RAM.mem_6);
    $readmemh (tp_hex_file[7], tb.duv.SimAXIMem_1.AXI4RAM.mem_7);
  end
  
endmodule // tb_freechips_boom2

