module tb;

  timeunit 1ns;
  timeprecision 10ps;

  logic clock, reset;
  localparam clk_hperiod = 1ns;

  initial begin
    clock = 1'b1;
    reset = 1'b1;
    #(300);
    reset = 1'b0;
  end

  always #(clk_hperiod) begin
    clock <= !clock;
  end

  TestHarness duv
    (
     .clock (clock),
     .reset (reset),
     .io_success ()
     );

  initial begin
	#(1000000);
    $finish;
  end

  always_ff @ (posedge clock) begin
    if (tb.duv.ExampleRocketSystem.debug_1.auto_dmInner_dmInner_tl_in_a_valid &&
        tb.duv.ExampleRocketSystem.debug_1.auto_dmInner_dmInner_tl_in_a_ready &&
        tb.duv.ExampleRocketSystem.debug_1.auto_dmInner_dmInner_tl_in_a_bits_address == 32'h108) begin
      $write ("ExapmleRocketSystem Debug Asserted. Finished.\n");
      $finish;
    end
  end

  logic [1023:0] tp_hex_file;
  initial begin
    void'($value$plusargs("tp_hex=%s", tp_hex_file));
	$readmemh (tp_hex_file, tb.duv.SimAXIMem.AXI4RAM.mem.mem_ext.ram);
  end

  
endmodule // tb

