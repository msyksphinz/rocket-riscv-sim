module tb_sifive_freedom_unleashed;

  timeunit 1ns;
  timeprecision 10ps;

  logic clock, reset;
  localparam clk_hperiod = 1ns;

  initial begin
    clock = 1'b1;
    reset = 1'b1;
    #(300);
    reset = 1'b0;
  end

  always #(clk_hperiod) begin
    clock <= !clock;
  end

  U500VC707DevKitSystem duv
    (
     .clock (clock),
     .reset (reset),

     .debug_systemjtag_jtag_TCK(1'b0),
     .debug_systemjtag_jtag_TMS(1'b0),
     .debug_systemjtag_jtag_TDI(1'b0),
     .debug_systemjtag_jtag_TDO_data(),
     .debug_systemjtag_reset(1'b0),
     .debug_ndreset(),
     .uart_0_txd(),
     .uart_0_rxd(1'b0),
     .spi_0_sck(),
     .spi_0_dq_0_i(1'b0),
     .spi_0_dq_0_o(),
     .spi_0_dq_1_i(1'b0),
     .spi_0_dq_1_o(),
     .spi_0_dq_2_i(1'b0),
     .spi_0_dq_2_o(),
     .spi_0_dq_3_i(1'b0),
     .spi_0_dq_3_o(),
     .spi_0_cs_0(),
     .gpio_0_pins_0_o_oval(),
     .gpio_0_pins_1_o_oval(),
     .gpio_0_pins_2_o_oval(),
     .gpio_0_pins_3_o_oval(),
     .xilinxvc707mig_ddr3_addr(),
     .xilinxvc707mig_ddr3_ba(),
     .xilinxvc707mig_ddr3_ras_n(),
     .xilinxvc707mig_ddr3_cas_n(),
     .xilinxvc707mig_ddr3_we_n(),
     .xilinxvc707mig_ddr3_reset_n(),
     .xilinxvc707mig_ddr3_ck_p(),
     .xilinxvc707mig_ddr3_ck_n(),
     .xilinxvc707mig_ddr3_cke(),
     .xilinxvc707mig_ddr3_cs_n(),
     .xilinxvc707mig_ddr3_dm(),
     .xilinxvc707mig_ddr3_odt(),
     .xilinxvc707mig_ddr3_dq(),
     .xilinxvc707mig_ddr3_dqs_n(),
     .xilinxvc707mig_ddr3_dqs_p(),
     .xilinxvc707mig_sys_clk_i(),
     .xilinxvc707mig_ui_clk(),
     .xilinxvc707mig_ui_clk_sync_rst(),
     .xilinxvc707mig_mmcm_locked(),
     .xilinxvc707mig_aresetn(1'b0),
     .xilinxvc707mig_sys_rst(1'b0),
     .xilinxvc707pcie_REFCLK_rxp(1'b0),
     .xilinxvc707pcie_REFCLK_rxn(1'b0),
     .xilinxvc707pcie_pci_exp_txp(),
     .xilinxvc707pcie_pci_exp_txn(),
     .xilinxvc707pcie_pci_exp_rxp(1'b0),
     .xilinxvc707pcie_pci_exp_rxn(1'b0),
     .xilinxvc707pcie_axi_aresetn(1'b0),
     .xilinxvc707pcie_axi_aclk_out(),
     .xilinxvc707pcie_axi_ctl_aclk_out(),
     .xilinxvc707pcie_mmcm_lock()
     );

    initial begin
	#(1000);
    $finish;
  end

endmodule // tb_sifive_freedom_unleashed
