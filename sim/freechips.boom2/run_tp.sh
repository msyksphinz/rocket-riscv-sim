#!/bin/sh

hex_lst=`ls -1 ../../program/benchmark/isa_boomv2/*.hex`
# hex_lst=`ls -1 ../../program/benchmark/isa_boomv2/lat_I_add.hex`

rm -rf results
mkdir -p results
rm -f results/result.log

for hex in ${hex_lst}
do
	log_name=`basename ${hex} .hex`
	echo "Running" `basename ${hex}` "..." ${log_name}
	make run_vcs TP_HEX=${hex} > /dev/null && 
	make dump_dasm > /dev/null &&
    cp dasm.log results/dasm_${log_name}.log
	start_cycle=`grep "cycle" dasm.log | sed -n "3p" | sed 's/  */ /g' | cut -f2 -d' '`
	stop_cycle=` grep "cycle" dasm.log | sed -n "4p" | sed 's/  */ /g' | cut -f2 -d' '`
	cycle=`expr ${stop_cycle} - ${start_cycle}`
	echo `basename ${hex}` ${start_cycle} ${stop_cycle} ${cycle} `echo "scale=5; ${cycle} / 1536.0" | bc` >> results/result.log
done
