# Get environment variables
if {[catch {set gui         $env(GUI)         }]} {set gui         "off"         }
if {[catch {set do_file     $env(DO_FILE)     }]} {set do_file     "off"         }
if {[catch {set cvfsm       $env(COVERAGE_FSM)}]} {set cvfsm       "off"         }
if {[catch {set dump        $env(DUMP)        }]} {set dump        "off"         }
if {[catch {set dump_start  $env(DUMP_START)  }]} {set dump_start  0             }
if {[catch {set dump_length $env(DUMP_LENGTH) }]} {set dump_length 0             }
if {[catch {set dump_hier   $env(DUMP_HIER)   }]} {set dump_hier   "/TestDriver" }
