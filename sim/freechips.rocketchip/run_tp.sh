#!/bin/sh

hex_lst=`ls -1 ../../program/benchmark/isa_rocket/*.hex`

rm -f result.log

for hex in ${hex_lst}
do
	echo "Running" `basename ${hex}` "..."
	make run_vcs TP_HEX=${hex} > /dev/null
	start_cycle=`grep "cycle" dasm.log | sed -n "3p" | sed 's/  */ /g' | cut -f2 -d' '`
	stop_cycle=` grep "cycle" dasm.log | sed -n "4p" | sed 's/  */ /g' | cut -f2 -d' '`
	cycle=`expr ${stop_cycle} - ${start_cycle}`
	echo `basename ${hex}` ${start_cycle} ${stop_cycle} ${cycle} `echo "scale=5; ${cycle} / 1536.0" | bc` >> result.log
done
