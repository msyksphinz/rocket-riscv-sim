#==========================================================
#
# PEZY Computing Confidential
#
# ---------------------------------------------------------
#                   Copyright (c) 2016 PEZY Computing, K.K.
#                                      All Rights Reserved.
#==========================================================

# source ../sim_common.tcl

#----------------------------------------------------------
# Proc
#----------------------------------------------------------

proc dump_fsdb {start length hier} {
  fsdbDumpfile out.fsdb
  fsdbDumpvars 0 $hier +mda +struct

  run $start ns
  if { $length == 0 } {
    run
  } else {
    run $length ns
    fsdbDumpoff
    run
  }
  quit
}

proc dump_vpd {start length hier} {
  run $start ns
  dump -file out.vpd
  dump -add $hier
  if { $length == 0 } {
    run
  } else {
    run $length ns
    dump -close
    run
  }
  quit
}

proc dump_saif {start length hier} {
  run $start ns
  power -gate_level all
  power $hier
  power -enable
  if { $length == 0 } {
    run
    power -disable
  } else {
    run $length ns
    power -disable
    run
  }
  power -report out.saif 1e-12 $hier
  quit
}

#----------------------------------------------------------
# Main
#----------------------------------------------------------

if {[catch {set gui         $env(GUI)        }]} {set gui         "off"    }
if {[catch {set do_file     $env(DO_FILE)    }]} {set do_file     "off"    }
if {[catch {set cvfsm       $env(COVERAGE_FSM)}]} {set cvfsm      "off"    }
if {[catch {set dump        $env(DUMP)       }]} {set dump        "off"    }
if {[catch {set dump_start  $env(DUMP_START) }]} {set dump_start  0        }
if {[catch {set dump_length $env(DUMP_LENGTH)}]} {set dump_length 0        }
if {[catch {set dump_hier   $env(DUMP_HIER)  }]} {set dump_hier   "/" }

# if { ![string match "off" $gui ] } {
#   if {[catch {set wave $env(WAVE)}]} { set wave "wave.tcl" }
#   if {[file exists $wave]} {source $wave}
# } elseif { ![string match "off" $do_file ] } {
#   do $do_file;quit
# } else {
#   switch $dump {
#     "fsdb"  { dump_fsdb $dump_start $dump_length $dump_hier }
#     "vpd"   { dump_vpd  $dump_start $dump_length $dump_hier }
#     "saif"  { dump_saif $dump_start $dump_length $dump_hier }
#     "off"   { run; quit }
#     default { puts "DUMP=$dump is not supported"; quit }
#   }
# }

dump_fsdb $dump_start $dump_length $dump_hier
