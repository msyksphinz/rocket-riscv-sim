VLOGAN_ARGS :=
VLOGAN_ARGS += $(addprefix +define+, $(DEFINES))
VLOGAN_ARGS += $(addprefix +incdir+, $(PATH_DIR))
VLOGAN_ARGS += -full64
VLOGAN_ARGS += -sverilog
VLOGAN_ARGS += +lint=TFIPC-L
VLOGAN_ARGS += -suppress=OPD -suppress=UII-L
VLOGAN_ARGS += -timescale=1ns/1ps
VLOGAN_ARGS += +libext+.v
VLOGAN_ARGS += -l vlog.log
VLOGAN_ARGS += -lca -kdb
VLOGAN_ARGS += +vcs+fsdbon
VLOGAN_ARGS += -debug_access+all

VCS_ARGS += +vcs+initreg+random
VCS_ARGS += -full64
VCS_ARGS += +lint=TFIPC-L
VCS_ARGS += -debug_access+all
VCS_ARGS += +transport_path_delays +transport_int_delays
VCS_ARGS += +pulse_e/100 +pulse_r/0 +pulse_int_e/100 +pulse_int_r/0
VCS_ARGS += +vcs+fsdbon

SIMV_ARGS += -ucli
SIMV_ARGS += +licq
SIMV_ARGS += +vcs+lic+wait
SIMV_ARGS += -do $(BASE_DIR)/sim/sim_vcs.tcl
SIMV_ARGS += -lca -kdb
SIMV_ARGS += +vcs+initreg+0
SIMV_ARGS += +maskromhex=$(HEX)
SIMV_ARGS += -l simv.log
SIMV_ARGS += +tp_hex_0=$(TP_HEX_0)
SIMV_ARGS += +tp_hex_1=$(TP_HEX_1)
SIMV_ARGS += +tp_hex_2=$(TP_HEX_2)
SIMV_ARGS += +tp_hex_3=$(TP_HEX_3)
SIMV_ARGS += +tp_hex_4=$(TP_HEX_4)
SIMV_ARGS += +tp_hex_5=$(TP_HEX_5)
SIMV_ARGS += +tp_hex_6=$(TP_HEX_6)
SIMV_ARGS += +tp_hex_7=$(TP_HEX_7)

compile_and_run_vcs: simv run_vcs

run_vcs: 
	./simv $(SIMV_ARGS) 2>&1 | tee sim.log
	make dump_dasm

compile_vcs: simv

simv: remove_filelist unzip_verilog filelist.f
	vlogan -full64 -sverilog \
		$(VLOGAN_ARGS) \
		-f filelist.f | tee compile.log
	vcs -full64 +lint=TFIPC-L \
		$(VCS_ARGS) \
		-top $(TB) 2>&1 | tee -a compile.log


verdi: ## run verdi
	verdi -workMode hardwareDebug -simflow -simBin ./simv -play ../run_verdi.tcl; \
