VLOG_ARGS :=
VLOG_ARGS += $(addprefix +define+, $(DEFINES))
VLOG_ARGS += $(addprefix +incdir+, $(PATH_DIR))
VLOG_ARGS += -sv
VLOG_ARGS += -work worklib
VLOG_ARGS += -l vlog.log
VLOG_ARGS += +initreg+0
VLOG_ARGS += +initmem+0
VLOG_ARGS += -timescale "1ns / 1ps"

VSIM_ARGS :=
VSIM_ARGS += +HEX=$(HEX)
VSIM_ARGS += +MAX_CYCLE=$(MAX_CYCLE)
VSIM_ARGS += worklib.$(TB)
VSIM_ARGS += -gui=off
VSIM_ARGS += +initreg+0
VSIM_ARGS += +initmem+0
VSIM_ARGS += +maskromhex=$(HEX)

ifeq ($(DUMP),yes)
VSIM_ARGS += +access+rw
VSIM_ARGS += -voptargs="+acc"
VSIM_ARGS += -do "add wave * -r; run -all; quit"
else
VSIM_ARGS += -do "run -all; quit"
endif

run_questa: $(TP_HEX) remove_filelist unzip_verilog filelist.f
	vlog $(VLOG_ARGS) `cat filelist.f` -l vlog.log
	vsim -c $(VSIM_ARGS) 2>&1 | tee sim.log
	make dump_dasm

gui_questa:
	vsim -gui vsim.wlf
